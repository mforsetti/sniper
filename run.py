import sniper
import os
import signal
import time


obj = None


def handler(snidata):
    print "{0} {2} > {3} {1} - {4}".format(snidata.src_mac,
                                           snidata.dst_mac,
                                           snidata.src_ip,
                                           snidata.dst_ip,
                                           snidata.hostname)


def cleanup(*args):
    obj.stop()
    time.sleep(1)


signal.signal(signal.SIGTERM, cleanup)

pid_file = "/opt/sniper/run/sniper.pid"
my_pid = os.getpid()
with open(pid_file, 'w') as pid_res:
    print >> pid_res, my_pid

# sniper.DEBUG = True
device = sniper.Device("eth3")
obj = sniper.Sniper(source=device)
obj.start(handler=handler)
