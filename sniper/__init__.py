import struct
import socket
import sys

try:
    import pcapy
except ImportError:
    print >> sys.stderr, "You'll need pcapy package for this module to work."
    pcapy = None
    sys.exit(1)


SKIP_ETHERTYPE = ['\x81\x00', '\x88\xa8', '\x91\x00']
DEBUG = False
IPv4 = 0x4
SSL_PORTS = [443, ]


def try_int(val, def_val=0):
    try:
        val = int(val)
    except ValueError:
        val = def_val
    return val


def b2l(inp, def_val=0):  # long, 4 bytes
    try:
        val = struct.unpack("!L", inp)[0]
    except struct.error:
        val = def_val
    return val


def b2h(inp, def_val=0):  # short, 2 bytes
    try:
        val = struct.unpack("!H", inp)[0]
    except struct.error:
        val = def_val
    return val


def b2b(inp, def_val=0):  # byte, 1 byte
    try:
        val = struct.unpack("!B", inp)[0]
    except struct.error:
        val = def_val
    return val


def b2s(inp, length, def_val=None):
    try:
        val = struct.unpack("!{0}s".format(length), inp)[0]
    except struct.error:
        val = def_val
    return val


def display_packet(packet, offset=0, length=65535, lb_cnt=16):
    temp = ""
    counter = 0

    my_packet = packet[offset:offset+length]
    for x in my_packet:
        temp += "{0:02X} ".format(x)
        counter += 1
        if counter >= lb_cnt:
            counter = 0
            temp += "\n"
    return temp


def parse_mac(raw_mac, separator=":"):
    temp = []
    for x in raw_mac:
        temp.append("{0:02X}".format(ord(x)))
    return separator.join(temp)


def parse_ip(raw_ip):
    return socket.inet_ntoa(raw_ip)


class Sniper(object):
    def __init__(self, source):
        if isinstance(source, Device) or isinstance(source, PcapFile):
            self.__source = source
        else:
            raise RuntimeError("Source to capture MUST BE a PCAP file or a capable network device.")

        self.__filter = "tcp port 443 or (vlan and ((vlan and tcp port 443) or tcp port 443))"
        self.__count = 0
        self.__current_count = 0
        self.__cap = None
        self.__callback = None
        self.__link_offset = 0
        self.__handler = None
        self.__stop = False

    def set_filter(self, my_filter):
        self.__filter = my_filter

    @staticmethod
    def list_devices():
        return pcapy.findalldevs()

    def __set_link_offset(self):
        datalink_type = self.__cap.datalink()

        if datalink_type == pcapy.DLT_RAW:
            self.__link_offset = 0
        elif datalink_type == pcapy.DLT_NULL or datalink_type == pcapy.DLT_PPP or datalink_type == pcapy.DLT_LOOP:
            self.__link_offset = 4
        elif datalink_type == pcapy.DLT_PPP_ETHER or datalink_type == pcapy.DLT_PPP_SERIAL:
            self.__link_offset = 8
        elif datalink_type == pcapy.DLT_EN10MB:
            self.__link_offset = 14
        elif datalink_type == pcapy.DLT_LINUX_SLL:
            self.__link_offset = 16
        elif datalink_type == pcapy.DLT_IEEE802_11:
            self.__link_offset = 32
        else:
            raise RuntimeError("Unknown or unsupported datalink type: {0}".format(datalink_type))
        
        if DEBUG:
            print "Link Offset: [ {0} ]".format(self.__link_offset)

    def stop(self):
        if DEBUG:
            print "!!! Stop flag set."
        self.__stop = True

    def start(self, handler, count=0):
        if callable(handler):
            self.__handler = handler
        else:
            raise RuntimeError("Handler must be a callable function.")
        
        self.__count = try_int(count)

        if isinstance(self.__source, Device):
            if DEBUG:
                print "Opening Device      : [ {0} ]".format(self.__source.device)
                print "        Snap Length : [ {0} ]".format(self.__source.snaplen)
                print "        Promiscuous : [ {0} ]".format("Yes" if self.__source.promisc else "No")
                print "        Timeout (ms): [ {0} ]".format(self.__source.to_ms)
            self.__cap = pcapy.open_live(self.__source.device,
                                         self.__source.snaplen,
                                         self.__source.promisc,
                                         self.__source.to_ms)
        else:
            if DEBUG:
                print "Opening File: [ {0} ]".format(self.__source.fn)
            self.__cap = pcapy.open_offline(self.__source.fn)

        if DEBUG:
            print "Filter: [ {0} ]".format(self.__filter)
        
        self.__cap.setfilter(self.__filter)

        while (self.__count == 0 or self.__current_count < self.__count) and not self.__stop:
            try:
                header, packet = self.__cap.next()
                if len(packet) == 0:
                    continue
                if DEBUG:
                    print "\n==============================================\n"
                if DEBUG:
                    print "\n\nPacket Length: [ {0} ]".format(len(packet))

                # we got packet. get mac address...
                raw_dst_mac = packet[0:6]
                raw_src_mac = packet[6:12]
                
                if DEBUG:
                    src_mac = SniData.parse_mac(raw_src_mac)
                    dst_mac = SniData.parse_mac(raw_dst_mac)
                    print "\nMAC ADDRESS"
                    print "SRC: [ {0} ]".format(src_mac)
                    print "DST: [ {0} ]".format(dst_mac)
                
                self.__set_link_offset()
                if self.__link_offset >= len(packet):
                    if DEBUG:
                        print "!! Link offset is larger than packet length!"
                    continue
                
                # counting IP packet offset...
                ip_test = packet[12:14] # get 2 bytes

                if ip_test in SKIP_ETHERTYPE: # a VLAN
                    if DEBUG:
                        print "\nVLAN {0}".format(hex(b2h(ip_test)))
                    self.__link_offset += 4
                    ip_test_qinq = packet[16:18]
                    if ip_test_qinq in SKIP_ETHERTYPE: # a QinQ
                        if DEBUG:
                            print "QinQ {0}".format(hex(b2h(ip_test_qinq)))
                        self.__link_offset += 4
                
                if DEBUG:
                    print "\nTotal IP Offset: [ {0} ]".format(self.__link_offset)
                
                if self.__link_offset >= len(packet):
                    if DEBUG:
                        print "!! Link offset is larger than packet length!"
                    continue
                
                # TODO: add IPv6 support.
                ip_header_length_bin = packet[self.__link_offset]
                ip_header_length = b2b(ip_header_length_bin) & 0xf
                if DEBUG:
                    print "\nIHL: [ {0} ] words (x4 bytes) = [ {1} ] bytes".format(ip_header_length,
                                                                                   ip_header_length * 4)
                
                # get IP address
                # for IPv4, offset is +12
                raw_src_ip = packet[self.__link_offset + 12:self.__link_offset + 16]
                raw_dst_ip = packet[self.__link_offset + 16:self.__link_offset + 20]
                if DEBUG:
                    print "\nSRC IP: [ {0} ]".format(SniData.parse_ip(raw_src_ip))
                    print "DST IP: [ {0} ]".format(SniData.parse_ip(raw_dst_ip))
                
                # start of tcp header is 4 x ip_header_length
                tcp_offset = self.__link_offset + (ip_header_length * 4)
                if DEBUG:
                    print "\nTCP Offset: [ {0} ]".format(tcp_offset)
                
                if tcp_offset >= len(packet):
                    if DEBUG:
                        print "!! TCP offset is larger than packet length!"
                    continue
                
                # we get TCP now. Parse its header right away !!
                raw_src_port = packet[tcp_offset:tcp_offset + 2]
                raw_dst_port = packet[tcp_offset + 2:tcp_offset + 4]
                
                src_port = SniData.parse_port(raw_src_port)
                dst_port = SniData.parse_port(raw_dst_port)
                
                if DEBUG:
                    print "\nSRC PORT: [ {0} ]".format(src_port)
                    print "DST PORT: [ {0} ]".format(dst_port)
                
                if src_port not in SSL_PORTS and dst_port not in SSL_PORTS:
                    if DEBUG:
                        print "!! Not a SSL port."
                    continue
                
                raw_data_offset = packet[tcp_offset + 12]
                data_offset = (b2b(raw_data_offset) >> 4)
                tcp_data_offset = tcp_offset + data_offset * 4
                
                if DEBUG:
                    print "\nDATA OFFSET: [ {0} ] words (x4 bytes) = [ {1} ] bytes".format(data_offset, data_offset * 4)
                    print "TCP DATA OFFSET: [ {0} ]".format(tcp_data_offset)
                
                if tcp_data_offset >= len(packet):
                    if DEBUG:
                        print "!! TCP data offset is larger than packet length!"
                    continue
                
                # check if it is a TLS Handshake packet...
                if DEBUG:
                    print "\nTLS TYPE: [ {0} ]".format(hex(ord(packet[tcp_data_offset])))

                if packet[tcp_data_offset] == '\x16':
                    if DEBUG:
                        print "is a TLS Handshake."
                        tls_type = packet[tcp_data_offset + 1:tcp_data_offset + 3]
                        tls_cat = {
                            '\x03\x00': "SSLv3",
                            '\x03\x01': "TLSv1",
                            '\x03\x02': "TLSv1.1",
                            '\x03\x03': "TLSv1.2"
                        }
                        print "\nTLS Type: [ {0} ]: {1}".format(hex(b2h(tls_type)),
                                                                tls_cat[tls_type] if tls_type in tls_cat else "N/A")
                    
                    if DEBUG:
                        print "\nHandshake Type: [ {0} ]".format(hex(ord(packet[tcp_data_offset + 5])))
                    
                    if packet[tcp_data_offset + 5] == '\x01':
                        raw_session_id_length = packet[tcp_data_offset + 43]
                        session_id_length = b2b(raw_session_id_length)
                        if DEBUG:
                            print "\nTLS Session ID Length: [ {0} ]".format(session_id_length)
                        
                        if tcp_data_offset >= len(packet):
                            if DEBUG:
                                print "!! TCP data offset is larger than packet length!"
                            continue
                        
                        # we'd like to skip TLS Session ID, so...
                        cipher_suite_offset = tcp_data_offset + 43 + 1 + session_id_length
                        raw_cipher_suite_length = packet[cipher_suite_offset:cipher_suite_offset + 2]
                        cipher_suite_length = b2h(raw_cipher_suite_length)
                        if DEBUG:
                            print "\nCipher Suite Length: [ {0} ]".format(cipher_suite_length)
                        
                        # now compression is always 2 bytes, so we can add it to extension_offset
                        extension_offset = cipher_suite_offset + 2 + cipher_suite_length + 2
                        if DEBUG:
                            print "\nExtension Offset: [ {0} ]".format(extension_offset)
                        
                        sni_hostname = None

                        while extension_offset <= len(packet):
                            raw_extension_length = packet[extension_offset:extension_offset + 2]
                            raw_extension_id = packet[extension_offset + 2:extension_offset + 4]
                            
                            extension_length = b2h(raw_extension_length)
                            
                            if DEBUG:
                                print "\nExtension Length: [ {0} ]".format(extension_length)
                                print "Extension ID: [ {0} ]".format(b2h(raw_extension_id))
                            
                            # TLS Extensions list:
                            # https://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml
                            if raw_extension_id == '\x00\x00':
                                raw_snl_length = packet[extension_offset + 4:extension_offset + 6]
                                snl_length = b2h(raw_snl_length)
                                if DEBUG:
                                    print "ServerNameList Length: [ {0} ]".format(snl_length)
                                
                                snl_offset = extension_offset + 6
                                
                                while snl_offset <= snl_offset + snl_length:
                                    raw_sn_length = packet[snl_offset : snl_offset + 2]
                                    raw_sn_type = packet[snl_offset + 2]
                                    
                                    sn_length = b2h(raw_sn_length)
                                    
                                    if DEBUG:
                                        print "Name Length: [ {0} ]".format(sn_length)
                                        print "Name Type: [ {0} ]".format(hex(b2b(raw_sn_type)))
                                    
                                    if raw_sn_type == '\x00':  # ServerName type is Hostname
                                        raw_hostname_length = packet[snl_offset + 3:snl_offset + 5]
                                        hostname_length = b2h(raw_hostname_length)
                                        raw_hostname = packet[snl_offset + 5:snl_offset + 5 + hostname_length]
                                        sni_hostname = b2s(raw_hostname, hostname_length)
                                        
                                        if DEBUG:
                                            print "SNI Hostname found: [ {0} ]".format(sni_hostname)
                                        
                                        if sni_hostname is not None:
                                            break
                                    
                                    snl_offset += sn_length + 2
                                break  # because we already got what we want.
                            
                            extension_offset += extension_length + 2
                        if sni_hostname is None:
                            continue
                        else:
                            self.__handler(SniData(raw_src_mac, raw_dst_mac, raw_src_ip, raw_dst_ip, sni_hostname))
                            if self.__count > 0:
                                self.__current_count += 1
                        
                    else:  # not a ClientHello
                        continue
                else:
                    continue
            except KeyboardInterrupt:
                break
            except IndexError:
                if DEBUG:
                    print "\nIndexError occured. Request is longer than packet."
                continue

        if not self.__stop:
            self.__stop = False


class Device(object):
    def __init__(self, device, snaplen=65536, promisc=True, to_ms=0):
        self.device = device
        self.snaplen = snaplen
        self.promisc = promisc
        self.to_ms = to_ms


class PcapFile(object):
    def __init__(self, fn):
        self.fn = fn


class SniData(object):
    def __init__(self, src_mac, dst_mac, src_ip, dst_ip, hostname):
        self.src_mac = self.parse_mac(src_mac)
        self.dst_mac = self.parse_mac(dst_mac)
        self.src_ip = self.parse_ip(src_ip)
        self.dst_ip = self.parse_ip(dst_ip)
        self.hostname = hostname
    
    @staticmethod
    def parse_mac(raw_mac):
        return parse_mac(raw_mac, ":")
    
    @staticmethod
    def parse_ip(raw_ip):
        return parse_ip(raw_ip)
    
    @staticmethod
    def parse_port(raw_port):
        return b2h(raw_port)
